package com.instil.webflix.basket.service;

import com.instil.webflix.basket.model.VoucherDescriptor;
import com.instil.webflix.basket.model.Basket;

public interface VoucherDescriptorFactory {
    VoucherDescriptor create(Basket basket);
}
