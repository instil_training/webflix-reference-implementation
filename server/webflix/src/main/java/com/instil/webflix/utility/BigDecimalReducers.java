package com.instil.webflix.utility;

import java.math.BigDecimal;
import java.util.function.BinaryOperator;

public class BigDecimalReducers {
    private static class BigDecimalSumReducer implements BinaryOperator<BigDecimal> {
        @Override
        public BigDecimal apply(BigDecimal bigDecimal, BigDecimal bigDecimal2) {
            return bigDecimal.add(bigDecimal2);
        }
    }

    public static BinaryOperator<BigDecimal> sum() {
        return new BigDecimalSumReducer();
    }
}
