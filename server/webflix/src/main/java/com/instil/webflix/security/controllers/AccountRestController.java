package com.instil.webflix.security.controllers;

import com.instil.webflix.security.data.AccountRepository;
import com.instil.webflix.security.exception.InvalidRegistrationException;
import com.instil.webflix.security.model.Account;
import com.instil.webflix.security.model.LoginResponse;
import com.instil.webflix.security.service.AccountLoginService;
import com.instil.webflix.security.service.AccountRegistrationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/account")
public class AccountRestController {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountLoginService accountLoginService;

    @Autowired
    private AccountRegistrationService accountRegistrationService;

    @RolesAllowed("ADMIN")
    @RequestMapping(method = GET, value = "/all", produces = "application/json")
    public Iterable<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    @RequestMapping(method = POST, value = "/login", produces = "application/json")
    public ResponseEntity<LoginResponse> login(@RequestHeader(value = "username") String email,
                                               @RequestHeader(value = "password") String password) {
        if (email == null || password == null) {
            logger.info("Invalid login request");
            return new ResponseEntity<>((LoginResponse) null, HttpStatus.BAD_REQUEST);
        }

        LoginResponse response = accountLoginService.login(email, password);
        return ResponseEntity.ok(response);
    }

    @RequestMapping(method = POST, value = "/register", produces = "application/json")
    public ResponseEntity<Object> register(@RequestBody() Account newAccount) throws InvalidRegistrationException {
        if (newAccount == null) {
            logger.info("Invalid registration");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        logger.info("Registering new user '" + newAccount.getEmailAddress());
        accountRegistrationService.register(newAccount);
        return ResponseEntity.ok().build();
    }
}
