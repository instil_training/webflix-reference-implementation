package com.instil.webflix.basket.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class VoucherCodeNotFoundException extends Exception {
    public VoucherCodeNotFoundException(String code) {
        super("Voucher '" + code + "' not recognised");
    }
}
