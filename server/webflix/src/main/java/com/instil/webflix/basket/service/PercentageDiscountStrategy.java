package com.instil.webflix.basket.service;

import com.instil.webflix.basket.model.Basket;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PercentageDiscountStrategy implements DiscountStrategy {
    private final int discountPercentage;
    private final BigDecimal scalar;

    public PercentageDiscountStrategy(int discountPercentage) {
        this.discountPercentage = discountPercentage;
        this.scalar = new BigDecimal(discountPercentage).divide(new BigDecimal(100));
    }

    @Override
    public BigDecimal calculateDiscount(Basket basket) {
        BigDecimal totalPriceForMovies = BasketUtility.getTotalMoviesPrice(basket);
        return totalPriceForMovies.multiply(scalar)
                    .setScale(2, RoundingMode.DOWN);
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }
}
