package com.instil.webflix.basket.data;

import com.instil.webflix.basket.model.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VoucherRepository extends JpaRepository<Voucher, Long> {
    Optional<Voucher> findOneByCode(String code);
}
