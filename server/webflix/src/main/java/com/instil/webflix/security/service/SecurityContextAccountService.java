package com.instil.webflix.security.service;

import com.instil.webflix.security.data.AccountRepository;
import com.instil.webflix.security.exception.InvalidRegistrationException;
import com.instil.webflix.security.model.Account;
import com.instil.webflix.security.model.LoginResponse;
import com.instil.webflix.utility.StringValidationUtility;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SecurityContextAccountService implements CurrentAccountService, AccountLoginService, AccountRegistrationService {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private Roles roles;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private StringValidationUtility validator;

    public Account getCurrent() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String email = authentication.getName();
            logger.info("Finding account of current user: " + email);
            return accountRepository.findByEmailAddress(email)
                    .orElseThrow(() -> new UsernameNotFoundException("Could not find the user '" + email + "'"));
        }

        logger.info("Anonymous request for current user");
        throw new BadCredentialsException("Anonymous user access not permitted");
    }

    public LoginResponse login(String email, String password) {
        logger.info("Setting authentication context");
        final Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(email, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        logger.info("Authenticated " + email);
        return constructLoginResponse(email);
    }

    public void register(Account newAccount) throws InvalidRegistrationException {
        ensureNewAccountIsValid(newAccount);

        newAccount.setPassword(passwordEncoder.encode(newAccount.getPassword()));
        newAccount.getRoles().add(roles.getUserRole());
        accountRepository.save(newAccount);
    }

    private void ensureNewAccountIsValid(Account newAccount) throws InvalidRegistrationException {
        if (!validator.isValidEmail(newAccount.getEmailAddress())) {
            throw new InvalidRegistrationException("Invalid email format");
        }

        if (!validator.isValidPassword(newAccount.getPassword())) {
            throw new InvalidRegistrationException("Invalid password format");
        }

        if (accountRepository.findByEmailAddress(newAccount.getEmailAddress()) != null) {
            throw new InvalidRegistrationException("Email address already present on system");
        }
    }

    private LoginResponse constructLoginResponse(String email) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);

        final String token = tokenUtil.generateToken(userDetails);
        final List<String> roles = userDetails.getAuthorities().stream()
                .map(Object::toString)
                .collect(Collectors.toList());

        return new LoginResponse(token, roles);
    }
}