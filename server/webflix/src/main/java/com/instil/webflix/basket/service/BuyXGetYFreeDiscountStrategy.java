package com.instil.webflix.basket.service;

import com.instil.webflix.basket.model.Basket;
import com.instil.webflix.basket.model.BasketItem;
import com.instil.webflix.movies.model.Movie;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;

import static com.instil.webflix.utility.BigDecimalReducers.sum;
import static java.util.stream.Collectors.toList;

public class BuyXGetYFreeDiscountStrategy implements DiscountStrategy {
    private final int purchaseItems;
    private final int freeItems;

    public BuyXGetYFreeDiscountStrategy(int purchaseItems, int freeItems) {
        this.purchaseItems = purchaseItems;
        this.freeItems = freeItems;
    }
    @Override
    public BigDecimal calculateDiscount(Basket basket) {
        Collection<Movie> movies = getMoviesForBasketSortedByCheapest(basket);
        int numberOfDiscounts = movies.size() / (purchaseItems + freeItems);

        BigDecimal totalForCheapestMovies = movies.stream()
                .limit(numberOfDiscounts * freeItems)
                .map(Movie::getPrice)
                .reduce(BigDecimal.ZERO, sum());

        return totalForCheapestMovies;
    }

    private Collection<Movie> getMoviesForBasketSortedByCheapest(Basket basket) {
        return basket.getItems().stream()
                .map(BasketItem::getMovie)
                .sorted(Comparator.comparing(Movie::getPrice))
                .collect(toList());
    }

    public int getPurchaseItems() {
        return purchaseItems;
    }

    public int getFreeItems() {
        return freeItems;
    }
}
