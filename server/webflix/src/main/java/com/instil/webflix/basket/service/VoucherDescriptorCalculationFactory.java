package com.instil.webflix.basket.service;

import com.instil.webflix.basket.exception.UnknownDiscountException;
import com.instil.webflix.basket.model.Voucher;
import com.instil.webflix.basket.model.VoucherDescriptor;
import com.instil.webflix.basket.model.Basket;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class VoucherDescriptorCalculationFactory implements VoucherDescriptorFactory {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private DiscountStrategyFactory discountStrategyFactory;

    @Override
    public VoucherDescriptor create(Basket basket) {
        Voucher voucher = basket.getVoucher();
        if (voucher == null) {
            return null;
        }

        try {
            DiscountStrategy strategy = discountStrategyFactory.create(voucher.getOffer());
            BigDecimal discount = strategy.calculateDiscount(basket);
            return new VoucherDescriptor(voucher.getOffer(), discount);
        }
        catch (UnknownDiscountException e) {
            // Should never happen if DB is correct
            logger.error("Could not parse offer code - " + voucher.getOffer());
            return null;
        }
    }
}
