package com.instil.webflix.basket.service;

import com.instil.webflix.basket.data.BasketItemRepository;
import com.instil.webflix.basket.data.BasketRepository;
import com.instil.webflix.basket.data.VoucherRepository;
import com.instil.webflix.basket.exception.VoucherCodeNotFoundException;
import com.instil.webflix.basket.model.BasketSummary;
import com.instil.webflix.basket.model.Voucher;
import com.instil.webflix.basket.model.VoucherDescriptor;
import com.instil.webflix.basket.model.Basket;
import com.instil.webflix.basket.model.BasketItem;
import com.instil.webflix.movies.model.Movie;
import com.instil.webflix.security.data.AccountRepository;
import com.instil.webflix.security.model.Account;

import java.util.Collection;

import static java.util.stream.Collectors.*;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DbBasketService implements BasketService {
	private final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired
	private BasketRepository basketRepository;

	@Autowired
	private BasketItemRepository basketItemRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private VoucherRepository voucherRepository;

	@Autowired
	private VoucherDescriptorFactory voucherDescriptorFactory;

	@Override
	public int getItemCount(Account account) {
		Basket basket = getBasketForAccount(account);
		return basket.getItems().size();
	}

	@Override
	public void addMovieToBasket(Account account, Movie movie) {
		Basket basket = getBasketForAccount(account);
		if (!basketContainsMovie(basket, movie)) {
			basket.getItems().add(new BasketItem(basket, movie));
			basketRepository.save(basket);
		}
	}

	@Override
	public void clearBasket(Account account) {
		Basket basket = getBasketForAccount(account);
		basketRepository.delete(basket);
	}
	@Override
	public BasketSummary getSummary(Account account) {
		Basket basket = getBasketForAccount(account);
		Collection<Movie> moviesForBasket = getMoviesForBasket(basket);
		BigDecimal totalCostForMovies = BasketUtility.getTotalMoviesPrice(basket);
		VoucherDescriptor voucher = voucherDescriptorFactory.create(basket);
		if (voucher != null) {
			totalCostForMovies = totalCostForMovies.subtract(voucher.getDiscount());
		}

		return new BasketSummary(moviesForBasket, voucher, totalCostForMovies);
	}

	@Override
	public void clearMovie(Account account, Long movieId) {
		Basket basket = getBasketForAccount(account);
		basket.getItems().stream()
				.filter(x -> x.getMovie().getId().equals(movieId))
				.findFirst()
				.ifPresent(x ->  {
					logger.info("Deleting basket item " + x.getId());
					x.setBasket(null);
					basketItemRepository.delete(x);
				});
	}

	@Override
	public void applyVoucher(Account account, String code) throws VoucherCodeNotFoundException {
		logger.info("Applying voucher " + code);
		Voucher voucher = voucherRepository.findOneByCode(code)
				.orElseThrow(() -> new VoucherCodeNotFoundException(code));

		Basket basket = getBasketForAccount(account);
		basket.setVoucher(voucher);
		basketRepository.save(basket);
	}

	@Override
	public void checkout(Account account) {
		Basket basket = getBasketForAccount(account);
		account.getMyMovies().addAll(
				basket.getItems().stream()
					.map(BasketItem::getMovie)
					.collect(toList()));

		accountRepository.save(account);
		basketRepository.delete(basket);
	}

	private boolean basketContainsMovie(Basket basket, Movie movie) {
		return basket.getItems().stream()
				.map(BasketItem::getMovie)
				.anyMatch(x -> x.getId().equals(movie.getId()));
	}

	private Collection<Movie> getMoviesForBasket(Basket basket) {
		return basket.getItems().stream()
				.map(BasketItem::getMovie)
				.collect(toList());
	}
	
	private Basket getBasketForAccount(Account account) {
		Basket basket = basketRepository.findByAccount(account);
		if (basket == null) {
			logger.info("Could not find basket for account: " + account.getEmailAddress());
			return new Basket(account);
		}
		return basket;
	}
}
