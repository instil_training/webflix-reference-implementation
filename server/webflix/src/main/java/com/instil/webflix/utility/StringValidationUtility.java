package com.instil.webflix.utility;

public interface StringValidationUtility {
    boolean isValidEmail(String emailAddress);

    boolean isValidPassword(String password);
}
