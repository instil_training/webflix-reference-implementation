package com.instil.webflix.basket.model;

import java.math.BigDecimal;

public class VoucherDescriptor {
    private final String name;
    private final BigDecimal discount;

    public String getName() {
        return name;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public VoucherDescriptor(String name, BigDecimal discount) {
        this.name = name;
        this.discount = discount;
    }
}
