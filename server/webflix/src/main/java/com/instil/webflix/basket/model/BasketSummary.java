package com.instil.webflix.basket.model;

import java.math.BigDecimal;

import com.instil.webflix.movies.model.Movie;

public class BasketSummary {
	private final Iterable<Movie> movies;
	private final VoucherDescriptor voucher;
	private final BigDecimal total;

	public BasketSummary(Iterable<Movie> movies, VoucherDescriptor voucher, BigDecimal total) {
		this.movies = movies;
		this.voucher = voucher;
		this.total = total;
	}

	public Iterable<Movie> getMovies() {
		return movies;
	}
	
	public BigDecimal getTotal() {
		return total;
	}

	public VoucherDescriptor getVoucher() {
		return voucher;
	}
}

