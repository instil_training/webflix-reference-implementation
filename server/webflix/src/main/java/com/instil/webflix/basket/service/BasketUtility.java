package com.instil.webflix.basket.service;

import com.instil.webflix.basket.model.Basket;
import com.instil.webflix.basket.model.BasketItem;
import com.instil.webflix.movies.model.Movie;

import java.math.BigDecimal;

import static com.instil.webflix.utility.BigDecimalReducers.sum;

public class BasketUtility {
    public static BigDecimal getTotalMoviesPrice(Basket basket) {
        return basket.getItems().stream()
                .map(BasketItem::getMovie)
                .map(Movie::getPrice)
                .reduce(BigDecimal.ZERO, sum());
    }
}
