package com.instil.webflix.security.service;

import com.instil.webflix.security.model.LoginResponse;

public interface AccountLoginService {
    LoginResponse login(String email, String password);
}
