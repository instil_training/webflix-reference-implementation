package com.instil.webflix.basket.exception;

public class UnknownDiscountException extends Exception {
    public UnknownDiscountException(String message) {
        super(message);
    }
}
