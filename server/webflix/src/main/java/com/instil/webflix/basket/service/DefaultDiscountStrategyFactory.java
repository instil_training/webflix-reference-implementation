package com.instil.webflix.basket.service;

import com.instil.webflix.basket.exception.UnknownDiscountException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class DefaultDiscountStrategyFactory implements DiscountStrategyFactory {
    @Override
    public DiscountStrategy create(String offer) throws UnknownDiscountException {
        try {
            String[] parts = offer.split(" ");
            if (isBugXGetYFree(parts)) {
                return new BuyXGetYFreeDiscountStrategy(
                        Integer.parseInt(parts[1]),
                        Integer.parseInt(parts[3])
                );
            } else if (isSpendXGetAmountOff(parts)) {
                return new SpendXGetAmountOffDiscountStrategy(
                        new BigDecimal(Integer.parseInt(removeCurrency(parts[1]))),
                        new BigDecimal(removeCurrency(parts[3]))
                );
            } else if (isSpendXGetPercentageOff(parts)) {
                return new SpendXGetPercentageOffDiscountStrategy(
                        new BigDecimal(Integer.parseInt(removeCurrency(parts[1]))),
                        Integer.parseInt(removePercentage(parts[3]))
                );
            } else if (isPercentageDiscount(parts)) {
                return new PercentageDiscountStrategy(
                        Integer.parseInt(removePercentage(parts[0]))
                );
            }
        } catch (NumberFormatException e) {
            // Ignore the format and report as an Invalid Parameter Exception below
        }

        throw new UnknownDiscountException("Unknown discount - " + offer);
    }

    private String removePercentage(String part) {
        return part.replace("%", "");
    }

    private String removeCurrency(String part) {
        return part.replace("£", "");
    }

    private boolean isSpendXGetAmountOff(String[] parts) {
        return isSpendXGetYOff(parts) && parts[3].contains("£");
    }

    private boolean isSpendXGetPercentageOff(String[] parts) {
        return isSpendXGetYOff(parts) && parts[3].contains("%");
    }

    private boolean isSpendXGetYOff(String[] parts) {
        return parts.length == 5 &&
                parts[0].equals("SPEND") &&
                parts[2].equals("GET") &&
                parts[4].equals("OFF");
    }

    private boolean isPercentageDiscount(String[] parts) {
        return parts.length == 2 &&
                parts[1].equals("DISCOUNT");
    }

    private boolean isBugXGetYFree(String[] parts) {
        return parts.length == 5 &&
                parts[0].equals("BUY") &&
                parts[2].equals("GET") &&
                parts[4].equals("FREE");
    }
}
