package com.instil.webflix.basket.service;

import com.instil.webflix.basket.model.Basket;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SpendXGetPercentageOffDiscountStrategy implements DiscountStrategy {
    private final BigDecimal threshold;
    private final int discountPercentage;
    private final BigDecimal scalar;

    public SpendXGetPercentageOffDiscountStrategy(BigDecimal threshold, int discountPercentage) {
        this.threshold = threshold;
        this.discountPercentage = discountPercentage;
        this.scalar = new BigDecimal(discountPercentage).divide(new BigDecimal(100));
    }
    @Override
    public BigDecimal calculateDiscount(Basket basket) {
        BigDecimal totalForBasket = BasketUtility.getTotalMoviesPrice(basket);
        if (totalForBasket.compareTo(threshold) >= 0) {
            return totalForBasket.multiply(scalar)
                    .setScale(2, RoundingMode.DOWN);
        }

        return BigDecimal.ZERO;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }

    public BigDecimal getThreshold() {
        return threshold;
    }
}
