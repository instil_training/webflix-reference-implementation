package com.instil.webflix.basket.service;

import com.instil.webflix.basket.model.Basket;

import java.math.BigDecimal;

public class SpendXGetAmountOffDiscountStrategy implements DiscountStrategy {
    private final BigDecimal threshold;

    private final BigDecimal saving;
    public SpendXGetAmountOffDiscountStrategy(BigDecimal threshold, BigDecimal saving) {
        this.threshold = threshold;
        this.saving = saving;
    }

    @Override
    public BigDecimal calculateDiscount(Basket basket) {
        BigDecimal totalForBasket = BasketUtility.getTotalMoviesPrice(basket);
        if (totalForBasket.compareTo(threshold) >= 0) {
            return saving;
        }

        return BigDecimal.ZERO;
    }

    public BigDecimal getThreshold() {
        return threshold;
    }

    public BigDecimal getSaving() {
        return saving;
    }
}
