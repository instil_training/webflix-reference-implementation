package com.instil.webflix.basket.service;

import com.instil.webflix.basket.exception.UnknownDiscountException;

public interface DiscountStrategyFactory {
    DiscountStrategy create(String offer) throws UnknownDiscountException;
}
