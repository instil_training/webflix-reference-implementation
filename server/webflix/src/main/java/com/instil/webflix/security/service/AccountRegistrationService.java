package com.instil.webflix.security.service;

import com.instil.webflix.security.exception.InvalidRegistrationException;
import com.instil.webflix.security.model.Account;

public interface AccountRegistrationService {
    void register(Account newAccount) throws InvalidRegistrationException;
}
