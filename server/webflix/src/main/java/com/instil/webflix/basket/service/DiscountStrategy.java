package com.instil.webflix.basket.service;

import com.instil.webflix.basket.model.Basket;

import java.math.BigDecimal;

public interface DiscountStrategy {
    BigDecimal calculateDiscount(Basket basket);
}
