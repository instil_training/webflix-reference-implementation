package com.instil.webflix.basket;

import com.instil.webflix.basket.model.Basket;
import com.instil.webflix.basket.service.BuyXGetYFreeDiscountStrategy;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class BuyXGetYFreeDiscountStrategyTest {

    private BuyXGetYFreeDiscountStrategy target;
    private Basket basket;

    @Before
    public void setUp() {
        basket = new StubBasketBuilder()
                    .withPrices(1, 2, 1, 3, 5, 10)
                    .build();
    }

    @Test
    public void shouldNotFailRemovingDiscountFromEmptyBasket() {
        basket = new StubBasketBuilder().build();

        target = new BuyXGetYFreeDiscountStrategy(1, 1);

        assertThat(target.calculateDiscount(basket), is(equalTo(BigDecimal.ZERO)));
    }

    @Test
    public void shouldSumCheapestMoviesInBuy1Get1Free() {
        final BigDecimal expectedResult = new BigDecimal(4); // 1 + 1 + 2 (see data above)

        target = new BuyXGetYFreeDiscountStrategy(1, 1);

        assertThat(target.calculateDiscount(basket), is(equalTo(expectedResult)));
    }

    @Test
    public void shouldSumCheapestMoviesInBuy2Get1Free() {
        final BigDecimal expectedResult = new BigDecimal(2); // 1 + 1 (see data above)

        target = new BuyXGetYFreeDiscountStrategy(2, 1);

        assertThat(target.calculateDiscount(basket), is(equalTo(expectedResult)));
    }

    @Test
    public void shouldSumCheapestMoviesInBuy3Get1Free() {
        final BigDecimal expectedResult = new BigDecimal(1); // (see data above)

        target = new BuyXGetYFreeDiscountStrategy(3, 1);

        assertThat(target.calculateDiscount(basket), is(equalTo(expectedResult)));
    }

    @Test
    public void shouldSumCheapestMoviesInBuy5Get1Free() {
        final BigDecimal expectedResult = new BigDecimal(1); // (see data above)

        target = new BuyXGetYFreeDiscountStrategy(5, 1);

        assertThat(target.calculateDiscount(basket), is(equalTo(expectedResult)));
    }

    @Test
    public void shouldNotHaveAnyDiscountIfNotEnoughQualifyingPurchases() {
        final BigDecimal expectedResult = BigDecimal.ZERO; // (see data above)

        target = new BuyXGetYFreeDiscountStrategy(6, 1);

        assertThat(target.calculateDiscount(basket), is(equalTo(expectedResult)));
    }

    @Test
    public void shouldSumMultipleCheapestMoviesInBuy1Get2Free() {
        final BigDecimal expectedResult = new BigDecimal(7); // 1 + 1 + 2 + 3(see data above)

        target = new BuyXGetYFreeDiscountStrategy(1, 2);

        assertThat(target.calculateDiscount(basket), is(equalTo(expectedResult)));
    }

}
