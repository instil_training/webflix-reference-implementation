package com.instil.webflix.utility;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class SimpleStringValidationUtilityTest {
    private SimpleStringValidationUtility target;

    @Before
    public void setUp() throws Exception {
        target = new SimpleStringValidationUtility();
    }

    @Test
    public void shouldReportEmailAsValid() {
        assertThat("Short email", target.isValidEmail("a@a.co"), is(true));
        assertThat("Medium", target.isValidEmail("abc@abc.com"), is(true));
        assertThat("Underscores", target.isValidEmail("abc_some@abcdef.com"), is(true));
        assertThat("Full stops", target.isValidEmail("abc.some@abc.def.com"), is(true));
        assertThat("Long", target.isValidEmail("qwertyuiop.asd_fghjkl123@abcdef-dewdwedwed.com"), is(true));
    }

    @Test
    public void shouldReportPasswordAsInvalidIfLessThan8Characters() {
        assertThat("Empty string",
                target.isValidPassword(""), is(false));
        assertThat("Only 1 character",
                target.isValidPassword("1"), is(false));
        assertThat("Boundary case - 7 letters with valid content",
                target.isValidPassword("aB34567"), is(false));
    }

    @Test
    public void shouldReportPasswordAsInvalidIfDoesNotContainLowerCaseLetters() {
        assertThat(target.isValidPassword("ASDFASD3"), is(false));
    }

    @Test
    public void shouldReportPasswordAsInvalidIfDoesNotContainUpperCaseLetters() {
        assertThat(target.isValidPassword("abcdefg3"), is(false));
    }

    @Test
    public void shouldReportPasswordAsInvalidIfDoesNotContainDigits() {
        assertThat(target.isValidPassword("abcdefgH"), is(false));
    }

    @Test
    public void shouldReportValidPasswordWhenUppercaseAndLowerCaseAndNumbersAnd8OrMoreCharacters() {
        assertThat("Only 1 digit",
                target.isValidPassword("aBcdefg8"), is(true));
        assertThat("Only 1 lowercase",
                target.isValidPassword("aBCDEFG8"), is(true));
        assertThat("More than 8 characters",
                target.isValidPassword("aBcdefg8sdcadsdcasdc"), is(true));
        assertThat("Boundary case - 8 characters",
                target.isValidPassword("aB345678"), is(true));
    }
}