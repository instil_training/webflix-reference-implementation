package com.instil.webflix.basket;

import com.instil.webflix.basket.model.Basket;
import com.instil.webflix.basket.service.PercentageDiscountStrategy;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class PercentageDiscountStrategyTest {

    private PercentageDiscountStrategy target;
    private Basket basket;

    @Before
    public void setUp() {
        basket = new StubBasketBuilder()
                .withPrices(1, 2, 1, 3, 5, 10, 8)
                .build();
    }

    @Test
    public void shouldNotFailRemovingDiscountFromEmptyBasket() {
        basket = new StubBasketBuilder().build();

        target = new PercentageDiscountStrategy(10);

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(BigDecimal.ZERO)));
    }

    @Test
    public void shouldCalculate10PercentDiscountCorrectly() {
        BigDecimal expectedResult = new BigDecimal(3); // See data above

        target = new PercentageDiscountStrategy(10);

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(expectedResult)));
    }

    @Test
    public void shouldCalculate50PercentDiscountCorrectly() {
        BigDecimal expectedResult = new BigDecimal(15); // See data above

        target = new PercentageDiscountStrategy(50);

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(expectedResult)));
    }

    @Test
    public void shouldRoundDownDiscountToNearestPenny() {
        basket = new StubBasketBuilder()
                .withPrices("0.10", "0.10", "0.10")
                .build();
        BigDecimal expectedResult = new BigDecimal("0.01"); // Rounded down from 0.015

        target = new PercentageDiscountStrategy(5);

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(expectedResult)));
    }
}
