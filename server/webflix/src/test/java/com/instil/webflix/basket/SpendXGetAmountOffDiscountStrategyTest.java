package com.instil.webflix.basket;

import com.instil.webflix.basket.model.Basket;
import com.instil.webflix.basket.service.SpendXGetAmountOffDiscountStrategy;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class SpendXGetAmountOffDiscountStrategyTest {
    private SpendXGetAmountOffDiscountStrategy target;

    private Basket basket;

    @Before
    public void setUp() {
        basket = new StubBasketBuilder()
                .withPrices(10, 2, 3, 5, 10)
                .build();
    }

    @Test
    public void shouldNotFailRemovingDiscountFromEmptyBasket() {
        basket = new StubBasketBuilder().build();

        target = new SpendXGetAmountOffDiscountStrategy(BigDecimal.TEN, BigDecimal.ONE);

        assertThat(target.calculateDiscount(basket), is(equalTo(BigDecimal.ZERO)));
    }

    @Test
    public void shouldReportDiscountIfCostIsAtThreshold() {
        target = new SpendXGetAmountOffDiscountStrategy(
                new BigDecimal(30),
                new BigDecimal(5));

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(new BigDecimal(5))));
    }

    @Test
    public void shouldReportDiscountIfCostIsOverThreshold() {
        target = new SpendXGetAmountOffDiscountStrategy(
                new BigDecimal(10),
                new BigDecimal(4));

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(new BigDecimal(4))));
    }

    @Test
    public void shouldNotHaveAnyDiscountIfCostIsBelowThreshold() {
        target = new SpendXGetAmountOffDiscountStrategy(
                new BigDecimal("30.01"),
                new BigDecimal(5));

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(BigDecimal.ZERO)));
    }
}
