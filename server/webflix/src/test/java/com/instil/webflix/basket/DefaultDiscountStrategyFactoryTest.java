package com.instil.webflix.basket;

import com.instil.webflix.basket.exception.UnknownDiscountException;
import com.instil.webflix.basket.service.*;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class DefaultDiscountStrategyFactoryTest {

    private DefaultDiscountStrategyFactory target;

    @Before
    public void setUp() throws Exception {
        target = new DefaultDiscountStrategyFactory();
    }

    @Test
    public void shouldConstructBuy1Get1FreeStrategy() throws Exception {
        DiscountStrategy result = target.create("BUY 1 GET 1 FREE");

        assertThat("Correct type", result, is(instanceOf(BuyXGetYFreeDiscountStrategy.class)));

        BuyXGetYFreeDiscountStrategy typedRsult = (BuyXGetYFreeDiscountStrategy)result;
        assertThat("Purchase Items", typedRsult.getPurchaseItems(), is(1));
        assertThat("Free Items", typedRsult.getFreeItems(), is(1));
    }

    @Test
    public void shouldConstructBuy3Get2FreeStrategy() throws Exception {
        DiscountStrategy result = target.create("BUY 3 GET 2 FREE");

        assertThat("Correct type", result, is(instanceOf(BuyXGetYFreeDiscountStrategy.class)));

        BuyXGetYFreeDiscountStrategy typedRsult = (BuyXGetYFreeDiscountStrategy)result;
        assertThat("Purchase Items", typedRsult.getPurchaseItems(), is(3));
        assertThat("Free Items", typedRsult.getFreeItems(), is(2));
    }

    @Test
    public void shouldConstruct10PercentDiscount() throws Exception {
        DiscountStrategy result = target.create("10% DISCOUNT");

        assertThat("Correct type", result, is(instanceOf(PercentageDiscountStrategy.class)));

        PercentageDiscountStrategy typedRsult = (PercentageDiscountStrategy)result;
        assertThat(typedRsult.getDiscountPercentage(), is(10));
    }

    @Test
    public void shouldConstruct15PercentDiscount() throws Exception {
        DiscountStrategy result = target.create("15% DISCOUNT");

        assertThat("Correct type", result, is(instanceOf(PercentageDiscountStrategy.class)));

        PercentageDiscountStrategy typedRsult = (PercentageDiscountStrategy)result;
        assertThat(typedRsult.getDiscountPercentage(), is(15));
    }

    @Test
    public void shouldConstructSpend10Get15PercentDiscount() throws Exception {
        DiscountStrategy result = target.create("SPEND £10 GET 15% OFF");

        assertThat("Correct type", result, is(instanceOf(SpendXGetPercentageOffDiscountStrategy.class)));

        SpendXGetPercentageOffDiscountStrategy typedRsult = (SpendXGetPercentageOffDiscountStrategy)result;
        assertThat(typedRsult.getThreshold(), is(comparesEqualTo(new BigDecimal(10))));
        assertThat(typedRsult.getDiscountPercentage(), is(15));
    }

    @Test
    public void shouldConstructSpend15Get10PercentDiscount() throws Exception {
        DiscountStrategy result = target.create("SPEND £15 GET 10% OFF");

        assertThat("Correct type", result, is(instanceOf(SpendXGetPercentageOffDiscountStrategy.class)));

        SpendXGetPercentageOffDiscountStrategy typedRsult = (SpendXGetPercentageOffDiscountStrategy)result;
        assertThat(typedRsult.getThreshold(), is(comparesEqualTo(new BigDecimal(15))));
        assertThat(typedRsult.getDiscountPercentage(), is(10));
    }

    @Test
    public void shouldConstructSpend10Get15Discount() throws Exception {
        DiscountStrategy result = target.create("SPEND £10 GET £15 OFF");

        assertThat("Correct type", result, is(instanceOf(SpendXGetAmountOffDiscountStrategy.class)));

        SpendXGetAmountOffDiscountStrategy typedRsult = (SpendXGetAmountOffDiscountStrategy)result;
        assertThat(typedRsult.getThreshold(), is(comparesEqualTo(new BigDecimal(10))));
        assertThat(typedRsult.getSaving(), is(comparesEqualTo(new BigDecimal(15))));
    }

    @Test
    public void shouldConstructSpend10GetFractionDiscount() throws Exception {
        DiscountStrategy result = target.create("SPEND £10 GET £2.50 OFF");

        assertThat("Correct type", result, is(instanceOf(SpendXGetAmountOffDiscountStrategy.class)));

        SpendXGetAmountOffDiscountStrategy typedRsult = (SpendXGetAmountOffDiscountStrategy)result;
        assertThat(typedRsult.getThreshold(), is(comparesEqualTo(new BigDecimal(10))));
        assertThat(typedRsult.getSaving(), is(comparesEqualTo(new BigDecimal("2.50"))));
    }

    @Test
    public void shouldConstructSpend15Get10Discount() throws Exception {
        DiscountStrategy result = target.create("SPEND £15 GET £10 OFF");

        assertThat("Correct type", result, is(instanceOf(SpendXGetAmountOffDiscountStrategy.class)));

        SpendXGetAmountOffDiscountStrategy typedRsult = (SpendXGetAmountOffDiscountStrategy)result;
        assertThat(typedRsult.getThreshold(), is(comparesEqualTo(new BigDecimal(15))));
        assertThat(typedRsult.getSaving(), is(comparesEqualTo(new BigDecimal(10))));
    }

    @Test(expected = UnknownDiscountException.class)
    public void shouldThrowUnknownDiscountExceptionForUnknownDiscount1() throws Exception {
        target.create("THIS 15 NOT 0 DISCOUNT");
    }

    @Test(expected = UnknownDiscountException.class)
    public void shouldThrowUnknownDiscountExceptionForBadlyFormattedSpendXGetYOffDiscount1() throws Exception {
        target.create("SPEND £X GET Y% OFF");
    }

    @Test(expected = UnknownDiscountException.class)
    public void shouldThrowUnknownDiscountExceptionForBadlyFormattedBuyXGetYDiscount1() throws Exception {
        target.create("BUY X GET 1 FREE");
    }

    @Test(expected = UnknownDiscountException.class)
    public void shouldThrowUnknownDiscountExceptionForBadlyFormattedBuyXGetYDiscount2() throws Exception {
        target.create("BUY 1 GET Y FREE");
    }

    @Test(expected = UnknownDiscountException.class)
    public void shouldThrowUnknownDiscountExceptionForBadlyFormattedPercentageDiscount() throws Exception {
        target.create("FAIL DISCOUNT");
    }
}
