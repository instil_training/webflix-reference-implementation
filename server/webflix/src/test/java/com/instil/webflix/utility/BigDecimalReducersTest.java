package com.instil.webflix.utility;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.function.BinaryOperator;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class BigDecimalReducersTest {
    @Test
    public void shouldAddTwoDecimalsForSumReducer() {
        BinaryOperator<BigDecimal> reducer = BigDecimalReducers.sum();

        assertSumResult(reducer, 0, 0, 0);
        assertSumResult(reducer, 1, 0, 1);
        assertSumResult(reducer, 0, 1, 1);
        assertSumResult(reducer, 123, 45, 168);
    }

    private void assertSumResult(BinaryOperator<BigDecimal> reducer, int op1, int op2, int result) {
        assertThat(reducer.apply(new BigDecimal(op1), new BigDecimal(op2)),
                   is(equalTo(new BigDecimal(result))));
    }
}
