package com.instil.webflix.basket;

import com.instil.webflix.basket.model.Basket;
import com.instil.webflix.basket.service.SpendXGetPercentageOffDiscountStrategy;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.is;

public class SpendXGetPercentageOffDiscountStrategyTest {
    private Basket basket;
    private SpendXGetPercentageOffDiscountStrategy target;

    @Before
    public void setUp() {
        basket = new StubBasketBuilder()
                .withPrices(10, 2, 3, 5, 10)
                .build();
    }

    @Test
    public void shouldNotFailRemovingDiscountFromEmptyBasket() {
        basket = new StubBasketBuilder().build();

        target = new SpendXGetPercentageOffDiscountStrategy(BigDecimal.TEN, 1);

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(BigDecimal.ZERO)));
    }

    @Test
    public void shouldCalculateDiscountIfCostIsAtThreshold() {
        target = new SpendXGetPercentageOffDiscountStrategy(
                new BigDecimal(30),
                10);

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(new BigDecimal(3))));
    }

    @Test
    public void shouldCalculateDiscountIfCostIsOverThreshold() {
        target = new SpendXGetPercentageOffDiscountStrategy(
                new BigDecimal(10),
                20);

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(new BigDecimal(6))));
    }

    @Test
    public void shouldRoundDiscountTDownToPenny() {
        basket = new StubBasketBuilder()
                .withPrices("0.66")
                .build();

        target = new SpendXGetPercentageOffDiscountStrategy(
                new BigDecimal("0.50"),
                10);

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(new BigDecimal("0.06"))));
    }

    @Test
    public void shouldCalculateNoDiscountIfCostIsBelowThreshold() {
        target = new SpendXGetPercentageOffDiscountStrategy(
                new BigDecimal("30.01"),
                20);

        assertThat(target.calculateDiscount(basket), is(comparesEqualTo(BigDecimal.ZERO)));
    }
}