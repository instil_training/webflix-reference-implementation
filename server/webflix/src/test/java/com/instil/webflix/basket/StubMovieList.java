package com.instil.webflix.basket;

import com.instil.webflix.movies.model.Movie;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class StubMovieList {
    public static Collection<Movie> fromPrices(int... prices) {
        return fromPrices(IntStream.of(prices)
                            .mapToObj(Integer::toString)
                            .collect(toList()));
    }

    public static Collection<Movie> fromPrices(String... prices) {
        return fromPrices(Arrays.asList(prices));
    }

    public static Collection<Movie> fromPrices(Iterable<String> prices) {
        Collection<Movie> movies = new ArrayList<>();
        for (String price : prices) {
            addMovie(movies, price, price);
        }

        return movies;
    }

    private static void addMovie(Collection<Movie> movies, String title, String price) {
        Movie movie = new Movie();
        movie.setTitle(title);
        movie.setPrice(new BigDecimal(price));
        movies.add(movie);
    }
}