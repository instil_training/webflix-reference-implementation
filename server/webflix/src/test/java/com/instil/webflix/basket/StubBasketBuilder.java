package com.instil.webflix.basket;

import com.instil.webflix.basket.model.Basket;
import com.instil.webflix.basket.model.BasketItem;
import com.instil.webflix.movies.model.Movie;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StubBasketBuilder {
    protected Basket basket;

    public StubBasketBuilder() {
        this.basket = mock(Basket.class);
        when(basket.getItems()).thenReturn(new ArrayList<>());
    }

    public StubBasketBuilder withMovies(Collection<Movie> movies) {
        basket = mock(Basket.class);
        List<BasketItem> basketItems = movies.stream().map(x -> new BasketItem(basket, x)).collect(toList());
        when(basket.getItems()).thenReturn(basketItems);
        return this;
    }

    public StubBasketBuilder withPrices(int ...prices) {
        return withMovies(StubMovieList.fromPrices(prices));
    }

    public StubBasketBuilder withPrices(String ...prices) {
        return withMovies(StubMovieList.fromPrices(prices));
    }

   public Basket build() {
        return basket;
   }
}
