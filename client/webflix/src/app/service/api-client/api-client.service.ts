/**
 * Created by mattmccomb on 15/11/2016.
 */
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import {Injectable} from '@angular/core';
import {User} from '../../model/user';
import {RestService} from './rest.service';

@Injectable()
export class ApiClient {
  constructor(private restService: RestService) {
  }

  login(email: string, password: string): Observable<any> {
    return this.restService.post('/account/login', false)
      .addHeader('username', email)
      .addHeader('password', password)
      .build()
      .map(response => {
        if (response.ok) {
          return response.json();
        }

        return '';
      }).catch(response => {
        console.log('Error registering user: ' + response.toString());
        return Observable.throw(response.json().message);
      });
  }

  register(user: User): Observable<boolean> {
    let body = JSON.stringify(user);
    return this.restService.post('/account/register', false)
      .setBody(body)
      .build()
      .map(() => true)
      .catch((response: Response) => {
        console.log('Error registering user: ' + response.toString());
        return Observable.throw(response.json().message);
      });
  }

  listUsers(): Observable<any> {
    return this.restService.get('/accounts/all')
      .build()
      .map((response: Response) => {
        return response.json();
      })
      .catch((response: Response) => {
        console.log('Error listing users: ' + response.toString());
        return Observable.throw(response.json().message);
      });
  }
}
