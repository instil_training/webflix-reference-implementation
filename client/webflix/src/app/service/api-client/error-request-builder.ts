import {ResponseContentType, Response, ResponseOptions} from '@angular/http';
import {RequestBuilder} from './rest.service';
import {Observable} from 'rxjs/Observable';


export class ErrorRequestBuilder extends RequestBuilder {
  constructor(private errorCode: number) {
    super();
  }

  setBody(body: any): RequestBuilder {
    return this;
  }

  setContentType(contentType: ResponseContentType): RequestBuilder {
    return this;
  }

  setPath(path: string): RequestBuilder {
    return this;
  }

  addHeader(key: string, value: string): RequestBuilder {
    return this;
  }

  build(): Observable<Response> {
    let response = new Response(new ResponseOptions());
    response.status = this.errorCode;

    return Observable.throw(response);
  }
}

