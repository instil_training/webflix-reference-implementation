
export enum HttpCode {
  SUCCESS = 200,

  UNAUTHORISED = 401
}
