import {Movie} from './movie';

export class BasketSummary {
  movies: Movie[];
  total: number;
  voucher: Voucher;

  static empty(): BasketSummary {
    let empty = new BasketSummary();
    empty.movies = [];
    empty.total = 0;
    return empty;
  }
}

export interface Voucher {
  name: string;
  discount: number;
}
